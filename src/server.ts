import Example from "./example";
import { AppException } from "./exceptions/exceptions";


try {
    const app = new Example();
    app.init();
} catch(error) {
    console.error((error as AppException).getMessage())
}

