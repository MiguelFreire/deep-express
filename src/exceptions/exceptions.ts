export abstract class AppException {
    protected name = 'AppException';
    protected type = '123';
    protected message = "App Exception Message"
    
    public getName(): string {
        return this.name;
    };
    public getMessage(): string {
        return this.message;
    };

    public getType(): string {
        return this.type;
    }

}

export class InvalidParamException extends AppException{

    constructor(paramType: string) {
        super();
        this.name = "InvalidParamException";
        this.message = `Parameter (${paramType}) has invalid value`;
    }
}

export class BootstrapException extends AppException {
    constructor(module: string) {
        super();
        this.name = "BootstrapException";
        this.message = `Module (${module}) not initialized`;
    }
}


export class PageBootstrapException extends AppException {
    constructor(module: string) {
        super();
        this.name = "PageBootstrapException";
        this.message = `Module (${module}) not initialized`;
    }
}

export class MandatoryParamException extends AppException {
    constructor(parameter: string) {
        super();
        this.name = "MandatoryParamException";
        this.message = `Parameter (${parameter}) is mandatory but is missing from request`;
    }
}

export class MissingMandatoryParamException extends AppException {
    constructor(parameter: string) {
        super();
        this.name = "MissingMandatoryParamException";
        this.message = `Parameter (${parameter}) is mandatory but is missing from page parameters list`;
    }
}

export class InvalidParamValueException extends AppException {
    constructor(parameter: string) {
        super();
        this.name = "InvalidParamValueException";
        this.message = `Parameter (${parameter}) has invalid value`;
    }
}

export class DuplicateRouteException extends AppException {
    constructor(route: string) {
        super();
        this.name = "DuplicateRouteException";
        this.message = `Route (${route}) has already been registred`;
    }
}

export class RouteOnInvalidNamespaceException extends AppException {
    constructor(route: string) {
        super();
        this.name = "RouteOnInvalidNamespaceException";
        this.message = `Route (${route}) is from another namespace`;
    }
}

export class CompositePageException extends AppException {
    constructor(parameter: string) {
        super();
        this.name = "CompositePageException";
        this.message = `Route (${parameter}) has invalid composite page class`;
    }
}




export const Exceptions = {
    AppException, 
    InvalidParamException, 
    BootstrapException
}