import { Application } from "express";
import express from "express";
import next from "next";
import Server from "next/dist/next-server/server/next-server";
import { AppMiddlewareRegistry } from "./registries/middleware";
import { ResourceRegistry } from "./registries/page";
import { BootstrapException, AppException, DuplicateRouteException } from "./exceptions/exceptions";
import { AppMiddlewareConstructor, NamespaceValidator } from "./middlewares/middleware";
import { PageConstructor, StaticResource, StaticResourceConstructor, Robots, ResourceConstructor, Homepage, Homepage2, IndexCompositePage, OtherPage } from "./pages/page";
import { RouteRegistry } from "./registries/routes";
import { StaticRegistry } from "./registries/static";


export type NextServer = Server;
export type ExpressServer = Application;

//export type PageRegister = Set<Page>; 

export default abstract class App {

    server: ExpressServer;

    next: NextServer;

    port: number;

    middlewares: AppMiddlewareRegistry;

    routes: RouteRegistry;

    resources: ResourceRegistry;

    constructor(dev = false, port = 3000) {
        this.server = express();
        this.next = next({dev});
        this.port = port;

        this.middlewares = new AppMiddlewareRegistry();
        this.routes = new RouteRegistry();
        this.resources = new ResourceRegistry();
        this.boostrap();
    }

    public boostrap() {
        this.registerMiddlewares();
        this.registerResources();

        this.server.use((error: AppException, req: express.Request, res: express.Response, next: express.NextFunction) => {
            res.status(500).send(error.getMessage());
        })
    }

    private onInit() {
        console.log("LISTENING PORT" + this.port);
    }

    public init() {
        this.server.listen(this.port, () => this.onInit())
    }

    protected registerMiddlewares() {
        if(!this.middlewares) throw new BootstrapException('Middlewares');

        this.middlewares.add(this.getMiddlewares());

        const middlewares = this.middlewares.getAll();

        for(let c of middlewares) {
            const middleware = new c();
            middleware.register(this.server);
        }
    }

    protected registerResources() {
        if(!this.resources) throw new BootstrapException("Resources");

        this.resources.add(this.getResources());

        const resources = this.resources.getAll();

        for(let c of resources) {
            const resource = new c(this.server, this.next, this);
            const route = resource.getRoute();
            console.log("registering, ", resource.getRoute())
            if(this.routes.has(route)) {
                throw new DuplicateRouteException(route);
            } else {
                this.routes.add(route);
            }
            
            resource.register();
        }
    }

    public getMiddlewares(): AppMiddlewareConstructor[] {
        return [
            
        ];
    }

    public getResources(): ResourceConstructor[] {
        return [];
    }

}