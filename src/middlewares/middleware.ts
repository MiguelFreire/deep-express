import express from "express";
import { pathToRegexp } from "path-to-regexp";
import { ExpressServer } from "../app";
import { Page, Resource, ResourceConstructor } from "pages/page";
import { MandatoryParamException, InvalidParamException, MissingMandatoryParamException, RouteOnInvalidNamespaceException } from "../exceptions/exceptions";
import { RootNamespace } from "../namespaces/namespace";
import { Parameter } from "parameters/parameter";
import RequestUtility from "../utils/request";

export abstract class Middleware {
    public abstract getName(): string;
    public abstract handler(req: express.Request, res: express.Response, next: express.NextFunction): void;
    
    constructor() {
        this.handler = this.handler.bind(this);
    }

    public register(server: ExpressServer) {
        server.use(this.getHandler());
    }

    public getHandler(): express.RequestHandler {
        return this.handler;
    }

    public isProd(): boolean {
        return false;
    }

    public getExceptionResources(): ResourceConstructor[]   {
        return [];
    }

    public bypassCrawlers() {
        return true;
    }
}

export abstract class AppMiddleware extends Middleware {
    public register(server: ExpressServer) {
        server.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
            if(this.bypassCrawlers() && (new RequestUtility(req)).isCrawler()) return next();

            const route = req.path;
            const regex = pathToRegexp(route);
            const resources = this.getExceptionResources();

            const bypassMiddleware = resources.some((resource: ResourceConstructor) => {
                return regex.test(resource.route);
            })
            
            if(bypassMiddleware) {
                return next();
            }

            return this.getHandler()(req,res,next);
        });
    }
}

export abstract class ResourceMiddleware extends Middleware {
    constructor(protected resource: Resource) {
        super();
    }

    public register(server: ExpressServer) {
        server.use(this.resource.getRoute(), this.getHandler());
    }
}

export class MandatoryQueryParams extends ResourceMiddleware {
    public getName() {
        return "Mantory Params Validator Middleware";
    }

    public handler(req: express.Request, res: express.Response, next: express.NextFunction): void {
        const query = req.query;
        const params = this.resource.getQueryParams();
        const mandatoryParams = this.resource.getMandatoryParams();

        for(let m of mandatoryParams) {
            if(!(m in params)) {
                throw new MissingMandatoryParamException(m);
            }
            
            if(!(m in query)) {
                throw new MandatoryParamException(m);
            }
        }

        next();
    }
}
export class ValidateParams extends ResourceMiddleware {
    public getName() {
        return "Params Validator Middleware";
    }

    public handler(req: express.Request, res: express.Response, next: express.NextFunction): void {
        
        let params: {[param: string]: Parameter} = {}

        const queryParams = Object.entries(this.resource.getQueryParams());
        
        const query = req.query;

        for(let [n, p] of queryParams) {
            if(n in query) {
                const param = new p(query[n] as string)
                if(!param.isValid()) throw new InvalidParamException(n);
                params[n] = param;
            }
        }

        const routeParams = Object.entries(this.resource.getRouteParams());

        const rParams = req.params;

        for(let [n, p] of routeParams) {
            if(n in rParams) {
                const param = new p(query[n] as string)
                if(!param.isValid()) throw new InvalidParamException(n);
                params[n] = param;
            }
        }

        this.resource.setParams(params);

        next();
    }
}

export class NamespaceValidator extends ResourceMiddleware {
    public getName() {
        return "Namespace Validator Middleware";
    }

    public handler(req: express.Request, res: express.Response, next: express.NextFunction): void {
        const n = this.resource.getNamespace();
        if(!n || n instanceof RootNamespace) return next();

        const namespace = new n(req);
        if(!namespace.inNamespace()) {
            throw new RouteOnInvalidNamespaceException(req.path);
        }

        next();
    }
}

export type MiddlewareConstructor = new () => Middleware;
export type AppMiddlewareConstructor = new () => AppMiddleware;
export type ResourceMiddlewareConstructor = new (resource: Resource) => ResourceMiddleware;