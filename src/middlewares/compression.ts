import { AppMiddleware, Middleware } from "./middleware";
import compression from "compression";
import express from "express";
import { ExpressServer } from "app";

export default class Compression extends Middleware {
  public getName() {
    return "Compression";
  }

  public handler(req: express.Request, res: express.Response, next: express.NextFunction) {}

  public getHandler() {
    return compression();
  }
}