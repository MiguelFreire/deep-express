import express from "express";

export default class RequestUtility {

    private userAgent: string;

    private subdomains: string[];

    private route: string;

    private path: string;

    constructor(req: express.Request) {
        this.userAgent = req.get('user-agent') || 'Unknown';
        this.subdomains = req.hostname.split('.');
        this.route = req.route;
        this.path = req.path;
    }

    public getUserAgent() {
        return this.userAgent;
    }

    public getSubdomains() {
        return this.subdomains;
    }

    public getRoute() {
        return this.route;
    }

    public getPath() {
        return this.path;
    }
    public isMobileAgent() {
        const agent = this.getUserAgent();
        
        return  /\b(BlackBerry|webOS|iPhone|IEMobile)\b/i.test(agent) || 
        /\b(Android|Windows Phone|iPad|iPod)\b/i.test(agent)
    }

    public isCrawler() {
        const agent = this.getUserAgent();

        return /aptoide|bot|google|baidu|bing|msn|duckduckbot|teoma|slurp|yandex/i.test(agent);
    }
}