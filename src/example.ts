import App from "./app";
import { Homepage, OtherPage, Robots } from "./pages/page";
import { Middleware } from "./middlewares/middleware";
import express from "express";
import Compression from "./middlewares/compression";

class ExampleMiddleware extends Middleware {
    public  getName() {
        return 'Example';
    }

    public handler(req: express.Request, res: express.Response, next: express.NextFunction) {
        console.log("Middleware Example");

        next();
    }

    public getExceptionResources() {
        return [
            Homepage
        ]
    }
}

export default class Example extends App {
    constructor() {
        super(true);
    }

    public getResources() {
        return [
            Homepage,
            Robots,
        ]
    }

    public getMiddlewares() {
        return [
            ExampleMiddleware,
            Compression
        ]
    }
}