import { RegexParam } from "./regex";

export class WordParam extends RegexParam {
    public getRegex() {
        return new RegExp("^[A-Zaz]$")
    }

    public getName() {
        return "Word";
    }
}