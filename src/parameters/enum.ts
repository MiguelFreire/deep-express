import { Parameter } from "./parameter";

export class EnumParam extends Parameter{
    public getName() {
        return "Enum";
    }

    public getPossibleValues(): string[] {
       return []; 
    }

    public isValid() {
        return this.getPossibleValues().includes(this.value);
    }
}