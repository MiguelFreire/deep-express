import { EnumParam } from "./enum";

export class SortParam extends EnumParam {
    public getPossibleValues() {
        return ['top', 'latest', 'trending'];
    }

    public getName() {
        return "Sort";
    }
}
