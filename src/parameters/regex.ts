import { Parameter } from "./parameter";

export class RegexParam extends Parameter {
    
    public getName() {
        return "Regex";
    }

    public getRegex() {
        return new RegExp("(.+)");
    }

    public isValid() {
        const r = this.getRegex();

        return r.test(this.value);
    }

    public parseValue() {
        return this.value;
    }
}