export type ParameterConstructor = new (value: string) => Parameter;

export abstract class Parameter {

    public abstract isValid(): boolean;

    public abstract getName(): string;

    public getValue(): string {
        return this.value || this.getDefault();
    };

    public getDefault(): string {
        return "";
    }

    constructor(protected value: string) {}
}


//Primitives





//Page
//attributes: name, controller function, cache mechanism, subdomain watcher
