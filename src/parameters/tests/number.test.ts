import { NumberParam } from "../number"

test("should return true for strings with int numbers", () => {
    expect(new NumberParam('123').isValid()).toBe(true)
})

test("should return true for strings with float numbers", () => {
    expect(new NumberParam('12.3').isValid()).toBe(true)
})

test("should return true for strings with alphanumeric characters", () => {
    expect(new NumberParam('12a3').isValid()).toBe(false)
})