import { NumberParam } from "./number";

export class IntegerParam extends NumberParam {
    public getName() {
        return "Integer";
    }

    public isValid() {
        return super.isValid() && Number.isInteger(Number(this.value))
    }
}