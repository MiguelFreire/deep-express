import { Parameter } from "./parameter";

export class NumberParam extends Parameter {
    public getName() {
        return "Number";
    }

    public isValid() {
        return !Number.isNaN((new Number(this.value)).valueOf());
    }
}