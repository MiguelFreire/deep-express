import Registery from "./registry";
import { PageConstructor, ResourceConstructor } from "../pages/page";


export class ResourceRegistry extends Registery<ResourceConstructor> {}