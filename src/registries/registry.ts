export default class Registry<T> {

    entries: Set<T>;

    public constructor() {
        this.entries = new Set<T>();
    }

    public has(entry: T) {
        return this.entries.has(entry);
    }

    public add(entries: T | T[]) {
        if(Array.isArray(entries)) {
            for(let entry of entries) {
                this.entries.add(entry);
            }

            return;
        }

        this.entries.add(entries);
    }

    public getAll(): T[] {
        return Array.from(this.entries.values())
    }
}