import Registery from "./registry";
import { StaticResourceConstructor } from "../pages/page";

export class StaticRegistry extends Registery<StaticResourceConstructor> {}