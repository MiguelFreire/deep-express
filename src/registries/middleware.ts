import Registery from "./registry";
import { MiddlewareConstructor, AppMiddlewareConstructor,  ResourceMiddlewareConstructor } from "../middlewares/middleware";

export class MiddlewareRegistry extends Registery<MiddlewareConstructor> {}
export class AppMiddlewareRegistry extends Registery<AppMiddlewareConstructor> {}
export class ResourceMiddlewareRegistry extends Registery<ResourceMiddlewareConstructor> {}