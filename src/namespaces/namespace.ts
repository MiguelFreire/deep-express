import express from "express";
import RequestUtility from "../utils/request";
import App, { NextServer, ExpressServer } from "../app";

export type NamespaceConstructor = new (req: express.Request) => Namespace;

export abstract class Namespace {

    constructor(protected req: express.Request) {}

    public abstract getName(): string;

    public abstract getNamespace(): string;

    public abstract inNamespace(): boolean;
}

export abstract class RouteBasedNamespace extends Namespace {
    constructor(protected req: express.Request) {
        super(req);
    }

    public inNamespace() {
        const request = new RequestUtility(this.req);
        const path = request.getPath();
        
        const namespace = this.getNamespace().replace('/', '//');
        const regex = new RegExp(`^${namespace}\/.+$`);

        return regex.test(path);
    }
}

export abstract class SubdomainBasedNamespace extends Namespace {
    constructor(protected req: express.Request) {
        super(req);
    }

    public inNamespace() {
        const request = new RequestUtility(this.req);
        const domains = request.getSubdomains();
        if(domains.length === 0) return false;

        const subdomain = domains[this.getSubdomainIndex()];
        
        return this.getNamespace() === subdomain;
    }

    public getSubdomainIndex() {
        return 0;
    }
}

export class RootNamespace extends Namespace {
    constructor(protected req: express.Request) {
        super(req);
    }

    public getName() {
        return "Root Namespace";
    }

    public getNamespace() {
        return "root";
    }

    public inNamespace() {
        return true;
    }

}

export class NamespaceExampleB extends RouteBasedNamespace {
    public getName() {
        return "foo";
    }

    public getNamespace() {
        return "/a"
    }
}

export class NamespaceExampleA extends SubdomainBasedNamespace {
    public getName() {
        return "bar";
    }

    public getNamespace() {
        return "a"
    }
}