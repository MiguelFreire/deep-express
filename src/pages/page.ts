import express, { RequestHandler } from "express";
import App, { ExpressServer, NextServer } from "../app";
import { ResourceMiddlewareRegistry } from "../registries/middleware";
import { ParameterConstructor, Parameter } from "../parameters/parameter";
import { NumberParam } from "../parameters/number";
import {
  MandatoryQueryParams,
  ValidateParams,
  ResourceMiddlewareConstructor,
  NamespaceValidator,
} from "../middlewares/middleware";
import {
  PageBootstrapException,
  CompositePageException,
} from "../exceptions/exceptions";
import path from "path";
import {
  Namespace,
  NamespaceConstructor,
  NamespaceExampleA,
  RootNamespace,
} from "../namespaces/namespace";

type Query = { [key: string]: string };
export type PageConstructor = new (
  server: ExpressServer,
  next: NextServer,
  app: App
) => Page;
export type StaticResourceConstructor = new (
  server: ExpressServer,
  next: NextServer,
  app: App
) => StaticResource;
export interface ResourceConstructor {
  new (
    server: ExpressServer,
    next: NextServer,
    app: App
  ): Resource;
  
  route: string;
}

export abstract class Resource {
  static route = ""; //This should be extracted to another class PageRegister

  middlewares: ResourceMiddlewareRegistry;

  params: { [name: string]: Parameter };

  constructor(
    protected server: ExpressServer,
    protected next: NextServer,
    protected app: App
  ) {
    this.controller = this.controller.bind(this);
    this.middlewares = new ResourceMiddlewareRegistry();
  }

  public register() {
    const route = this.getRoute();
    //Middlewares registry
    this.middlewares.add(this.getMiddlewares());
    this.middlewares.add(this.getBaseMiddlewares());
    this.registerMiddlewares();

    this.server.get(route, this.controller);
  }

  public abstract getResourceName(): string;

  public getRoute(): string {
    return (this.constructor as any).route;
  } 

  public getRouteName(): string {
    return "";
  }
  public getNamespace(): NamespaceConstructor {
    return undefined;
  }

  public getQueryParams(): { [index: string]: ParameterConstructor } {
    return {};
  }

  public getRouteParams(): { [index: string]: ParameterConstructor } {
    return {};
  }

  public getMandatoryParams(): string[] {
    return [];
  }

  public getMiddlewares(): ResourceMiddlewareConstructor[] {
    return [];
  }

  public getBaseMiddlewares(): ResourceMiddlewareConstructor[] {
    return [NamespaceValidator];
  }

  public setParams(params: { [name: string]: Parameter }) {
    this.params = params;
  }

  public getParams() {
    return this.params;
  }

  public getParsedParams() {
    const params = Object.entries(this.getParams());

    const parsed = params.map(([n, p]) => [n, p.getValue()]);

    return Object.fromEntries(parsed);
  }

  protected registerMiddlewares() {
    if (!this.middlewares) throw new PageBootstrapException("Middlewares");

    this.middlewares.add(this.getMiddlewares());

    const middlewares = this.middlewares.getAll();

    for (let c of middlewares) {
      const middleware = new c(this);
      middleware.register(this.server);
    }
  }

  public controller(req: express.Request, res: express.Response): any {
    return res.send("OK");
  }
}

export abstract class Page extends Resource {
  constructor(
    protected server: ExpressServer,
    protected next: NextServer,
    protected app: App
  ) {
    super(server, next, app);
  }

  public getGlobalMiddlewares() {
    return [NamespaceValidator];
  }

  public abstract getPage(): string;

  public controller(req: express.Request, res: express.Response): any {
    const params = this.getParsedParams();

    return this.next.render(req, res, this.getPage(), params);
  }
}

export abstract class CompositePage extends Resource {
  protected pageInstances: Page[];

  constructor(
    protected server: ExpressServer,
    protected next: NextServer,
    protected app: App
  ) {
    super(server, next, app);

    const pages = this.getPages();
    this.pageInstances = pages.map((p) => new p(server, next, app));
  }

  public register() {
    const route = this.getRoute();
    //Middlewares registry
    this.middlewares.add(this.getMiddlewares());
    this.middlewares.add(this.getBaseMiddlewares());
    this.registerMiddlewares();

    this.server.get(route, (req: express.Request, res: express.Response) => {
      let root: Page;

      for (let page of this.pageInstances) {
        //get and init namespace instance
        const namespace = page.getNamespace();
        const n = new namespace(req);
        //if its rootnamespace nothing to do here just save page as root in case of fallback
        if (n instanceof RootNamespace) {
          root = page;
          continue;
        }
        //is in correct namespace? load controller
        if (n.inNamespace()) {
          return page.controller(req, res);
        }
      }
      //fallback failed? panic
      if (!root) throw new CompositePageException(this.getRoute());
      return root.controller(req, res);
    });
  }

  public getPage() {
    return "";
  }

  public abstract getPages(): PageConstructor[];
}

export abstract class StaticResource extends Resource {
  constructor(
    protected server: ExpressServer,
    protected next: NextServer,
    protected app: App
  ) {
    super(server, next, app);
  }

  public abstract getStaticResource(): string;

  public controller(req: express.Request, res: express.Response): any {
    return res.sendFile(path.join(this.next.publicDir, this.getStaticResource()));
  }
}

export class Robots extends StaticResource {
  static route = '/robots.txt';
  
  constructor(
    protected server: ExpressServer,
    protected next: NextServer,
    protected app: App
  ) {
    super(server, next, app);
  }
  public getResourceName() {
    return "Robots";
  }

  public getStaticResource() {
    return '/robots.txt';
  }
}

export class Homepage extends Page {

  static route = '/';

  constructor(
    protected server: ExpressServer,
    protected next: NextServer,
    protected app: App
  ) {
    super(server, next, app);
  }

  public getResourceName() {
    return "Homepage";
  }

  public getRouteName() {
    return "homepage";
  }

  public getPage() {
    return "index";
  }

  public getNamespace() {
    return RootNamespace;
  }

  public controller(req: express.Request, res: express.Response): any {
    return res.send("Homepage A");
  }
}

export class Homepage2 extends Page {
  static route = '/';

  constructor(
    protected server: ExpressServer,
    protected next: NextServer,
    protected app: App
  ) {
    super(server, next, app);
  }

  public getResourceName() {
    return "Homepage 2";
  }

  public getRouteName() {
    return "homepage2";
  }

  public getPage() {
    return "other-page";
  }

  public getNamespace() {
    return NamespaceExampleA;
  }

  public controller(req: express.Request, res: express.Response): any {
    return res.send("Homepage B");
  }
}

export class IndexCompositePage extends CompositePage {
  constructor(
    protected server: ExpressServer,
    protected next: NextServer,
    protected app: App
  ) {
    super(server, next, app);
  }

  public getPages() {
    return [Homepage, Homepage2];
  }

  public getResourceName() {
    return "Composite Page";
  }
}

export class OtherPage extends Page {
  static route = '/b';

  constructor(
    protected server: ExpressServer,
    protected next: NextServer,
    protected app: App
  ) {
    super(server, next, app);
  }

  public getResourceName() {
    return "Page test";
  }

  public getRouteName() {
    return "pageteste";
  }

  public getPage() {
    return "other-page";
  }

  public getNamespace() {
    return NamespaceExampleA;
  }

  public controller(req: express.Request, res: express.Response): any {
    return res.send("WORKED!");
  }
}
